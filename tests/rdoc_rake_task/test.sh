#!/bin/bash

set -xe

echo "
source 'https://rubygems.org'

gem 'rake'
" >> Gemfile

echo "
# This is a test module to test documentation with rdoc
module HelloWorld
	# Outputs string 'HelloWorld!'
	def self.message
		puts 'HelloWorld!'
	end
end
" > test_file.rb

echo "
This is my README.md
" >> README.md

echo "
require 'rdoc/task'
RDoc::Task.new do |rdoc|
  rdoc.main = 'README.md'
	rdoc.rdoc_files.include('*.md', '*.rb')
end
" >> Rakefile

bundle --local

bundle exec rake rdoc

rm -rf Gemfile Gemfile.lock Rakefile test_file.rb doc/
