#!/bin/bash

set -xe

echo "
source 'https://rubygems.org'
" >> Gemfile

echo "
# This is a test module to test documentation with rdoc
module HelloWorld
	# Outputs string 'HelloWorld!'
	def self.message
		puts 'HelloWorld!'
	end
end
" > test_file.rb

bundle

bundle exec rdoc test_file.rb

rm -rf Gemfile Gemfile.lock test_file.rb doc/
