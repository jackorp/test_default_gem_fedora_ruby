#!/bin/bash

set -xe

RDOC_VERSION="6.3.0"

# Some arbitrary version for now. It should be distinct from the OS version.
gem install -N --no-wrappers rdoc --version "$RDOC_VERSION"

# We expect RDoc to be preferred
[[ $(ruby -rrdoc -e "p RDoc::VERSION") == "\"$RDOC_VERSION\"" ]]

gem uninstall rdoc --version "$RDOC_VERSION"
