#!/bin/bash

set -xe

echo "
source 'https://rubygems.org'
" >> Gemfile

bundle

bundle exec ruby -rrdoc -e "pp RDoc.inspect"

rm -f Gemfile Gemfile.lock
